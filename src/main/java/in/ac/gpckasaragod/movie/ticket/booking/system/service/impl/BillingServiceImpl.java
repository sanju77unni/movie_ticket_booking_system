/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.movie.ticket.booking.system.service.impl;


import in.ac.gpckasaragod.movie.ticket.booking.system.service.BillingService;
import in.ac.gpckasaragod.movie.ticket.booking.system.ui.model.data.BillingDetails;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */

public class BillingServiceImpl extends ConnectionServiceImpl implements BillingService {


    @Override
    public BillingDetails readBillingDetails(Integer id) {
        BillingDetails billingDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM BILLING_DETAILS WHERE ID=" + id;
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                
                
                int numberOfTicketsSold = resultSet.getInt("NUMBER_OF_TICKETS_SOLD");
                Date bookingDate = resultSet.getDate("BOOKING_DATE");
                Double totalAmount = resultSet.getDouble("TOTAL_AMOUNT");
                Integer movie = resultSet.getInt("MOVIE_ID");
              
                billingDetails = new BillingDetails(id,numberOfTicketsSold,bookingDate,totalAmount,movie);
            }
            return billingDetails;
        } catch (SQLException ex) {
             Logger.getLogger(BillingServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
         return billingDetails;
        }
    
     
    @Override
    public List<BillingDetails>getAllBillingDetailses(){
        List<BillingDetails> billingDetailses = new ArrayList<>();
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM BILLING_DETAILS";
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");               
                Integer numberOfTicketsSold = resultSet.getInt("NUMBER_OF_TICKETS_SOLD");
                Date bookingDate = resultSet.getDate("BOOKING_DATE");
                Double totalAmount = resultSet.getDouble("TOTAL_AMOUNT");
                Integer movie = resultSet.getInt("MOVIE_ID");
                
                BillingDetails billingDetail = new BillingDetails(id,numberOfTicketsSold,bookingDate,totalAmount,movie);
                billingDetailses.add(billingDetail);
            }
        }   catch (SQLException ex) {
                Logger.getLogger(BillingServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        return billingDetailses;
    }

    
    @Override
    public String saveBillingDetails( String numberOfTicketsSold, Date bookingDate, String totalAmount,Integer movie) {
       Connection connection;
        try {
            connection = getConnection();
            Statement statement = connection.createStatement();
                SimpleDateFormat dbdateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String formatedDate = dbdateFormat.format(bookingDate);
                
            String query = "INSERT INTO BILLING_DETAILS(NUMBER_OF_TICKETS_SOLD,BOOKING_DATE,TOTAL_AMOUNT,MOVIE_ID) VALUES ('" + numberOfTicketsSold + "','" + formatedDate + "','" + totalAmount + "','" + movie+ "')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";
            } else {
                return "Saved succesfully";
            }
        } catch (SQLException ex) {
           Logger.getLogger(BillingServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Save failed";
    }

   
    


    @Override
    public String deleteBillingDetailsForm(Integer id){
            try{
                Connection connection = getConnection();
                String query = "DELETE FROM BILLING_DETAILS WHERE ID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1,id);
                int delete = statement.executeUpdate();
                if(delete !=1)
                    return "Delete failed";
                else
                    return "Deleted succesfully";
            }catch (SQLException ex) {
                ex.printStackTrace();
                return "Delete failed";
            }
            }

  
   
    @Override
      public String updateBillingDetailsForm(int id, String numberOfTicketsSold, Date bookingDate, String totalAmount,Integer movieId) {
            try{
     Connection connection = getConnection();
    Statement statement = connection.createStatement();
    SimpleDateFormat dbdateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String formatedDate = dbdateFormat.format(bookingDate);
            
     String query = "UPDATE BILLING_DETAILS SET NUMBER_OF_TICKETS_SOLD ='"+ numberOfTicketsSold +"',BOOKING_DATE='"+ formatedDate +"',TOTAL_AMOUNT='"+ totalAmount +"',MOVIE_ID='"+ movieId +"'WHERE ID="+id;
     System.out.print(query);
 int update = statement.executeUpdate(query);
 if(update !=1)
     return "Update failed";
 else
     return "Updated succesfully";
    }catch(SQLException ex){
        return "Update failed";
    
}
    }

    
}
        
        
    


   

   
  
  
    
    
            
        
        
         