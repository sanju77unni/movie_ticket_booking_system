/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.movie.ticket.booking.system.service;


import in.ac.gpckasaragod.movie.ticket.booking.system.ui.model.data.MovieDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface MovieService {
    
 public String saveMovieDetails(String movieName,String screenType,String screenNumber,String morningShow,String noonShow,String matinee,String secondShow);
    public MovieDetails readMovieDetails(Integer id);
      public List<MovieDetails>getAllMovieDetailses();
    
     public String updateMovieDetailsForm(int id,String movieName, String screenType, String screenNumber, String morningShow, String noonShow, String matinee, String secondShow);
      public String deleteMovieDetailsForm(Integer id);
    
}
