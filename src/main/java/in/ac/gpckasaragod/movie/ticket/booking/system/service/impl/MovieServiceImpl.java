/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.movie.ticket.booking.system.service.impl;

import in.ac.gpckasaragod.movie.ticket.booking.system.service.MovieService;
import in.ac.gpckasaragod.movie.ticket.booking.system.ui.model.data.MovieDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class MovieServiceImpl extends ConnectionServiceImpl implements MovieService {

    @Override
      public MovieDetails readMovieDetails(Integer id) {
        MovieDetails movieDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM MOVIE_DETAILS WHERE ID=" + id;
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                String movieName = resultSet.getString("MOVIE_NAME");
                String screenType = resultSet.getString("SCREEN_TYPE");
                String screenNumber = resultSet.getString("SCREEN_NUMBER");
                String morningShow = resultSet.getString("MORNING_SHOW");
                String noonShow = resultSet.getString("NOON_SHOW");
                String matinee = resultSet.getString("MATINEE");
                String secondShow = resultSet.getString("SECOND_SHOW");
                

                movieDetails = new MovieDetails(id, movieName, screenType, screenNumber, morningShow, noonShow, matinee, secondShow);

            }
            return movieDetails;
        } catch (SQLException ex) {
            Logger.getLogger(MovieServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return movieDetails;
    }

    @Override
    public List<MovieDetails> getAllMovieDetailses() {
        List<MovieDetails> movieDetailses = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM MOVIE_DETAILS";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Integer id = resultSet.getInt("ID");
                String movieName = resultSet.getString("MOVIE_NAME");
                String screenType = resultSet.getString("SCREEN_TYPE");
                String screenNumber = resultSet.getString("SCREEN_NUMBER");
                String morningShow = resultSet.getString("MORNING_SHOW");
                String noonShow = resultSet.getString("NOON_SHOW");
                String matinee = resultSet.getString("MATINEE");
                String secondShow = resultSet.getString("SECOND_SHOW");
                
                MovieDetails movieDetail = new MovieDetails(id, movieName, screenType, screenNumber, morningShow, noonShow, matinee, secondShow);
                movieDetailses.add(movieDetail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MovieServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return movieDetailses;
    }

    @Override
    public String deleteMovieDetailsForm(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM MOVIE_DETAILS WHERE ID = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete != 1) {
                return "Delete failed";
            } else {
                return "Deleted succesfully";
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return "Delete failed";
        }
    }

    @Override
    public String saveMovieDetails(String movieName, String screenType, String screenNumber, String morningShow, String noonShow, String matinee, String secondShow) {
        Connection connection;
        try {
            connection = getConnection();
            Statement statement = connection.createStatement();

            String query = "INSERT INTO MOVIE_DETAILS(MOVIE_NAME,SCREEN_TYPE,SCREEN_NUMBER,MORNING_SHOW,NOON_SHOW,MATINEE,SECOND_SHOW) VALUES ('" + movieName + "','" + screenType + "','" + screenNumber + "','" + morningShow + "','" + noonShow + "','" + matinee + "','" + secondShow + "')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";
            } else {
                return "Saved succesfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(MovieServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Save failed";
    }

    @Override
    public String updateMovieDetailsForm(int id,String movieName, String screenType, String screenNumber, String morningShow, String noonShow, String matinee, String secondShow) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();

            String query = "UPDATE MOVIE_DETAILS SET MOVIE_NAME='" + movieName + "',SCREEN_TYPE='" + screenType + "',SCREEN_NUMBER='" + screenNumber + "',MORNING_SHOW='" + morningShow + "',NOON_SHOW='" + noonShow + "',MATINEE='" + matinee + "',SECOND_SHOW='" + secondShow + "' WHERE ID=" + id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "Update failed";
            } else {
                return "Updated succesfully";
            }
        } catch (SQLException ex) {
            return "Update failed";

        }
    }
}