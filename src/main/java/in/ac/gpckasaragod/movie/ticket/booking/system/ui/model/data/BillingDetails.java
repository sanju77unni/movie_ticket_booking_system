/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.movie.ticket.booking.system.ui.model.data;

import java.util.Date;

/**
 *
 * @author student
 */
public class BillingDetails{
    private Integer id;
    private Integer numberOfTicketsSold;
    private Date bookingDate;
    private Double totalAmount;
    private Integer movie;

    public BillingDetails(Integer id, Integer numberOfTicketsSold, Date bookingDate, Double totalAmount, Integer movie) {
        this.id = id;
        this.numberOfTicketsSold = numberOfTicketsSold;
        this.bookingDate = bookingDate;
        this.totalAmount = totalAmount;
        this.movie = movie;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumberOfTicketsSold() {
        return numberOfTicketsSold;
    }

    public void setNumberOfTicketsSold(Integer numberOfTicketsSold) {
        this.numberOfTicketsSold = numberOfTicketsSold;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getMovie() {
        return movie;
    }

    public void setMovie(Integer movie) {
        this.movie = movie;
    }
}