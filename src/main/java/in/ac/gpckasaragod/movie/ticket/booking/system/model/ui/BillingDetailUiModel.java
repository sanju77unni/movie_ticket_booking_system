/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.movie.ticket.booking.system.model.ui;

/**
 *
 * @author student
 */
public class BillingDetailUiModel {
    private Integer id;
     private String movieName;
    private String screenType;
    private String screenNumber;
    private String morningShow;
    private String noonShow;
    private String matinee;
    private String secondShow;
    private String numberOfTicketsSold;
    private String bookingDate;
    private String totalAmount;

    public BillingDetailUiModel(Integer id, String movieName, String screenType, String screenNumber, String morningShow, String noonShow, String matinee, String secondShow, String numberOfTicketsSold, String bookingDate, String totalAmount) {
        this.id = id;
        this.movieName = movieName;
        this.screenType = screenType;
        this.screenNumber = screenNumber;
        this.morningShow = morningShow;
        this.noonShow = noonShow;
        this.matinee = matinee;
        this.secondShow = secondShow;
        this.numberOfTicketsSold = numberOfTicketsSold;
        this.bookingDate = bookingDate;
        this.totalAmount = totalAmount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getScreenType() {
        return screenType;
    }

    public void setScreenType(String screenType) {
        this.screenType = screenType;
    }

    public String getScreenNumber() {
        return screenNumber;
    }

    public void setScreenNumber(String screenNumber) {
        this.screenNumber = screenNumber;
    }

    public String getMorningShow() {
        return morningShow;
    }

    public void setMorningShow(String morningShow) {
        this.morningShow = morningShow;
    }

    public String getNoonShow() {
        return noonShow;
    }

    public void setNoonShow(String noonShow) {
        this.noonShow = noonShow;
    }

    public String getMatinee() {
        return matinee;
    }

    public void setMatinee(String matinee) {
        this.matinee = matinee;
    }

    public String getSecondShow() {
        return secondShow;
    }

    public void setSecondShow(String secondShow) {
        this.secondShow = secondShow;
    }

    public String getNumberOfTicketsSold() {
        return numberOfTicketsSold;
    }

    public void setNumberOfTicketsSold(String numberOfTicketsSold) {
        this.numberOfTicketsSold = numberOfTicketsSold;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
     
    
    
    
    
    
}
