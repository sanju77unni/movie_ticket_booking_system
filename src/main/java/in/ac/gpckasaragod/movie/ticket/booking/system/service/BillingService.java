/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.movie.ticket.booking.system.service;

import in.ac.gpckasaragod.movie.ticket.booking.system.ui.model.data.BillingDetails;
import java.util.Date;


import java.util.List;

/**
 *
 * @author student
 */
public interface BillingService {
     public String saveBillingDetails( String numberOfTicketsSold, Date bookingDate, String totalAmount,Integer movieId);
   public BillingDetails readBillingDetails(Integer id);
     public List<BillingDetails>getAllBillingDetailses();
  
    public String updateBillingDetailsForm(int id, String numberOfTicketsSold, Date bookingDate, String totalAmount,Integer movieId);
      public String deleteBillingDetailsForm(Integer id);
}
